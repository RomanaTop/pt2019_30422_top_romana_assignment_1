package Controller;

import java.util.List;

import Model.Task;

public class ConcreteStrategyQueue implements Strategy {
@Override
	public void addTask(List<Server> servers, Task t) {
		// TODO Auto-generated method stub
		int minTasks = 100;
		for (Server server : servers) {
			if (server.getTask().size() < minTasks) {
				minTasks = server.getTask().size();
			}   
		}

		for (Server server : servers) {
			if (server.getTask().size() == minTasks) {
				server.addTask(t);
				t.setQueueNr(server.serverId);
				break;
			}
		}
	}
/*public static void main(String argv[]) {
	Server server1 = new Server(1);
	Server server2 = new Server(2);
	Server server3 = new Server(3);
	server1.addTask(new Task (1,1,1));
	server1.addTask(new Task (1,1,1));
	server1.addTask(new Task (1,1,1));
	server2.addTask(new Task (2,2,2));
	server2.addTask(new Task (3,3,3));
	server3.addTask(new Task (4,4,4));
	server3.addTask(new Task (5,5,5));
	server3.addTask(new Task (6,6,6));
	List<Server> servers = new ArrayList<Server>();
	servers.add(server1);
	servers.add(server2);
	servers.add(server3);
	ConcreteStrategyQueue c = new ConcreteStrategyQueue();
	c.addTask(servers,new Task(7,7,7));
	System.out.println(server2.toString());
	
	
}*/

}
